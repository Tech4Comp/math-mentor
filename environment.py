import copy
import itertools
import random
from collections import defaultdict

import gymnasium as gym
import numpy as np
from gymnasium.spaces import Box, Dict, MultiDiscrete, Discrete

from classes import Student, IntendedLearningOutcomeRelevance, Task, Concept, IntendedLearningOutcomeProgress, \
    LearningObjectiveAssignment

PD_ARRAY_LIST = [
            [1, 0, 0, 0, 0, 0],
            [1, 1, 0, 0, 0, 0],
            [1, 1, 1, 0, 0, 0],
            [1, 1, 1, 1, 0, 0],
            [1, 1, 1, 1, 1, 0],
            [1, 1, 1, 1, 1, 1]
        ]

KD_ARRAY_LIST = [
            [1, 0, 0, 0],
            [1, 1, 0, 0],
            [1, 1, 1, 0],
            [1, 1, 1, 1]
        ]

def generate_decreasing_numbers_from_one(array) -> list[float]:
    new_array = copy.deepcopy(array)
    for i in range(len(new_array)):
        if new_array[i] == 1:
            # If it's the first element, generate a random number between 0 and 1
            if i == 0:
                new_array[i] = random.random()
            else:
                # Generate a random number that is less than or equal to the previous number
                new_array[i] = random.uniform(0, new_array[i - 1])
    return new_array


class RecommendationEnvironment(gym.Env):
    def __init__(self, env_config):
        self.selected_task = 0

        self.use_cont_action_space = env_config.get("use_cont_action_space", False)

        # FROM 0 to 1 (influence of weight in relevance calculation)
        self.WEIGHT_IMPORTANCE_FACTOR = env_config.get("weight_importance_factor", 1)
        # FROM 0 to 1 (influence of priority in improvement calculation) - Only Priority Concepts are considered!
        self.PRIORITY_IMPORTANCE_FACTOR = env_config.get("priority_importance_factor", 1)
        # Relevance Factors (all rewards map from 0-1 so factors are used to scale them to the desired range)
        self.CONCEPT_RELEVANCE_FACTOR = env_config.get("concept_relevance_factor", 10)
        self.PD_RELEVANCE_FACTOR = env_config.get("pd_relevance_factor", 1)
        self.KD_RELEVANCE_FACTOR = env_config.get("kd_relevance_factor", 1)
        # Improvement Factors (all rewards map from 0-1 so factors are used to scale them to the desired range)
        self.PD_IMPROVEMENT_FACTOR = env_config.get("pd_improvement_factor", 1)
        self.KD_IMPROVEMENT_FACTOR = env_config.get("kd_improvement_factor", 1)
        # Bonus Factor (reward is 1 for each bonus achieved (ilo reached) so factor is used to scale it to the desired range)
        self.BONUS_FACTOR = env_config.get("bonus_factor", 1)

        self.NEW_TASK_FACTOR = env_config.get("new_factor", 1)
        self.DIFFICULTY_FACTOR = env_config.get("difficulty_factor", 1)
        self.STRESS_FACTOR = env_config.get("stress_factor", 1)

        self.NUMBER_OF_RECOMMENDATIONS = env_config.get("number_of_recommendations", 1)
        self.MAX_CONCEPTS_PER_ACTION = env_config.get("max_concepts_per_action", 5)

        self.all_tasks: list[Task] = sorted(env_config["all_tasks"], key=lambda task: task.id)
        self.all_concepts: list[Concept] = sorted(env_config["all_concepts"], key=lambda concept: concept.id)

        self.action_space = self._create_action_space()
        self.observation_space = self._create_observation_space()

        self.chooseRandom = env_config["chooseRandom"]
        if not self.chooseRandom:
            self.prio_list: list[IntendedLearningOutcomeRelevance] = env_config["prio_list"]
            self.student: Student = env_config["student"]
        self.reset()

    def _create_action_space(self):
        if self.use_cont_action_space:
            # create all combinations of concepts for the action space (no duplicates)
            self.valid_concept_combinations = [
                list(action) for action in itertools.combinations(range(len(self.all_concepts)),
                                                                  self.MAX_CONCEPTS_PER_ACTION)
            ]

            concept_id_space = Discrete(len(self.valid_concept_combinations))
            pd_mapping_space = MultiDiscrete([len(PD_ARRAY_LIST)] * self.MAX_CONCEPTS_PER_ACTION)
            kd_mapping_space = MultiDiscrete([len(KD_ARRAY_LIST)] * self.MAX_CONCEPTS_PER_ACTION)

            return Dict({
                "difficulty": Box(low=0.0, high=1.0, shape=()),
                "p_slip": Box(low=0.0, high=1.0, shape=()),
                "p_guess": Box(low=0.0, high=1.0, shape=()),
                "concept_ids": concept_id_space,
                "pd_mappings": pd_mapping_space,
                "kd_mappings": kd_mapping_space,
                "weights": Box(low=0.0, high=1.0, shape=(self.MAX_CONCEPTS_PER_ACTION,))
            })
        else:
            return Discrete(len(self.all_tasks))

    def _create_observation_space(self):
        return Dict({
            "used_tasks": Box(low=0, high=1, shape=(len(self.all_tasks),)),
            "challenge_level": Box(low=0.0, high=1.0, shape=()),
            "stress": Box(low=0.0, high=1.0, shape=()),
            "concepts": Dict({
                concept.id: Dict({
                    "priority": Box(low=0.0, high=1.0, shape=()),
                    "pd_mapping": Discrete(len(PD_ARRAY_LIST)),
                    "pd_progress": Box(low=0.0, high=1.0, shape=(6,)),
                    "kd_mapping": Discrete(len(KD_ARRAY_LIST)),
                    "kd_progress": Box(low=0.0, high=1.0, shape=(4,)),
                }) for concept in self.all_concepts
            })
        })

    def reset(self, *, seed=None, options=None):
        random.seed(seed)

        if self.chooseRandom:
            num_selections_prio = random.randint(0, round(len(self.all_concepts) / 4))#round(len(self.all_concepts) / 4) #random.randint(1,  5) # round(len(self.all_concepts) / 4))
            random_selection_prio = random.sample(self.all_concepts, num_selections_prio)

            self.prio_list: list[IntendedLearningOutcomeRelevance] = []
            for concept in random_selection_prio:
                # Find all tasks that include this concept
                tasks_with_concept = [task for task in self.all_tasks if
                                      concept.id in [loa.concept.id for loa in task.learning_objective_assignments]]

                # Get the taxonomy mappings of the concept for each task
                pd_mappings = [loa.process_dimension for task in tasks_with_concept for loa in
                               task.learning_objective_assignments if
                               loa.concept.id == concept.id]

                kd_mappings = [loa.knowledge_dimension for task in tasks_with_concept for loa in
                               task.learning_objective_assignments if
                               loa.concept.id == concept.id]

                # Select the maximum taxonomy mapping
                max_pd_mapping = max(pd_mappings, key=lambda x: sum(x))
                max_kd_mapping = max(kd_mappings, key=lambda x: sum(x))

                # Create a list of arrays that are less than or equal to the maximum taxonomy mapping
                valid_pd_arrays = [array for array in PD_ARRAY_LIST if
                                   all(i <= j for i, j in zip(array, max_pd_mapping))]
                valid_kd_arrays = [array for array in KD_ARRAY_LIST if
                                   all(i <= j for i, j in zip(array, max_kd_mapping))]

                # Randomly select an array from the list of valid arrays
                selected_pd_mapping = random.choice(valid_pd_arrays)
                selected_kd_mapping = random.choice(valid_kd_arrays)

                priority = random.uniform(0.1, 1.0)

                # Create an IntendedLearningOutcome instance and add it to the prio_list
                self.prio_list.append(IntendedLearningOutcomeRelevance(concept,
                                                                       selected_pd_mapping,
                                                                       selected_kd_mapping,
                                                                       priority))

            self.student = Student(
                random.uniform(0.6, 1.0),
                random.uniform(0.1, 1.0),
                [],
                random.uniform(0.1, 1.0)
            )

            num_solved_tasks = random.randint(0, round(len(self.all_tasks) / 4))
            #num_solved_tasks = 0
            random_selection_tasks = random.sample(self.all_tasks, num_solved_tasks)
            self.student.solved_tasks = random_selection_tasks

            for task in random_selection_tasks:
                for loa_t in task.learning_objective_assignments:
                    concept = loa_t.concept

                    if any(ilo.concept.id == concept.id for ilo in self.student.ilo_progress):
                        continue

                    tasks_with_concept = [task for task in random_selection_tasks if
                                          concept.id in [loa.concept.id for loa in task.learning_objective_assignments]]

                    pd_mappings = [loa.process_dimension for task in tasks_with_concept for loa in
                                   task.learning_objective_assignments if
                                   loa.concept.id == concept.id]

                    kd_mappings = [loa.knowledge_dimension for task in tasks_with_concept for loa in
                                   task.learning_objective_assignments if
                                   loa.concept.id == concept.id]

                    max_pd_mapping = max(pd_mappings, key=lambda x: sum(x))
                    max_kd_mapping = max(kd_mappings, key=lambda x: sum(x))

                    self.student.ilo_progress.append(IntendedLearningOutcomeProgress(concept,
                                                                                     generate_decreasing_numbers_from_one(
                                                                                         max_pd_mapping),
                                                                                     generate_decreasing_numbers_from_one(
                                                                                         max_kd_mapping)))

        self.actions_used: list[Task] = []

        self.reward_complete = 0

        self.concept_relevance_reward = 0
        self.pd_relevance_reward = 0
        self.kd_relevance_reward = 0

        self.reward_pd_improvement = 0
        self.reward_kd_improvement = 0
        self.reward_bonus = 0

        self.reward_new = 0
        self.reward_difficulty_adjustment = 0
        self.reward_stress = 0

        self.obs = defaultdict(dict)
        # Initialize an empty dictionary for concepts
        concepts_dict = {}

        # Iterate over all concepts
        for concept in self.all_concepts:
            # Initialize default values
            priority = 0.0
            pd_mapping = 0
            pd_progress = np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
            kd_mapping = 0
            kd_progress = np.array([0.0, 0.0, 0.0, 0.0])

            # Check if the concept is in prio_list
            for i, prio in enumerate(self.prio_list):
                if prio.concept.id == concept.id:
                    priority = prio.priority
                    pd_mapping = PD_ARRAY_LIST.index(prio.process_dimension)
                    kd_mapping = KD_ARRAY_LIST.index(prio.knowledge_dimension)
                    break

            # Check if the concept is in student.ilo_progress
            for ilo_progress in self.student.ilo_progress:
                if ilo_progress.concept.id == concept.id:
                    pd_progress = np.array(ilo_progress.process_dimension)
                    kd_progress = np.array(ilo_progress.knowledge_dimension)
                    break

            # Add the concept to the dictionary
            concepts_dict[concept.id] = {
                "priority": priority,
                "pd_mapping": pd_mapping,
                "pd_progress": pd_progress,
                "kd_mapping": kd_mapping,
                "kd_progress": kd_progress,
            }

        solved_tasks = np.zeros(len(self.all_tasks))
        for i, t in enumerate(self.all_tasks):
            if t in self.student.solved_tasks:
                solved_tasks[i] = 1

        # Update the observation dictionary
        self.obs.update({
            "used_tasks": solved_tasks,
            "challenge_level": np.array(self.student.challenge_level),
            "stress": np.array(self.student.current_stress),
            "concepts": concepts_dict,
        })

        return self.obs, {}

    def step(self, action, benchmark=False):
        if self.use_cont_action_space:
            if not benchmark:
                selected_task = self.create_abstract_task_from_action(action)
            else:
                selected_task = action
        else:
            selected_task = self.all_tasks[action]

            # Reward new Tasks
            if self.obs["used_tasks"][action] != 1:
                self.reward_new = 1 * self.NEW_TASK_FACTOR
            self.obs["used_tasks"][action] = 1

        # set action used
        self.actions_used.append(selected_task)

        # get success rate
        rate = self.student.estimate_recommendation(selected_task)

        # adjust stress level
        self.student.adjust_stress(rate)
        self.obs["stress"] = np.array(self.student.current_stress)

        # calc competency
        student_before = copy.deepcopy(self.student.ilo_progress)
        self.student.update_progress_bkt(selected_task, rate)

        # update obs
        for ilo_progress in self.student.ilo_progress:
            self.obs["concepts"][ilo_progress.concept.id]["pd_progress"] = np.array(
                ilo_progress.process_dimension)
            self.obs["concepts"][ilo_progress.concept.id]["kd_progress"] = np.array(
                ilo_progress.knowledge_dimension)

        # REWARDS ###################################################

        # Reward task relevance
        matching_concepts = 0
        concept_relevance = 0
        pd_relevance = 0
        kd_relevance = 0
        for task_loa in selected_task.learning_objective_assignments:
            prio_ilo = next((ilo for ilo in self.prio_list if ilo.concept.id == task_loa.concept.id),
                              IntendedLearningOutcomeRelevance(task_loa.concept, [0] * 6, [0] * 4, 0))

            matching_concepts += 1

            adjusted_weight = 1 - (self.WEIGHT_IMPORTANCE_FACTOR * (1 - task_loa.weight))
            adjusted_priority = 1 - (self.PRIORITY_IMPORTANCE_FACTOR * (1 - prio_ilo.priority))

            if prio_ilo.priority > 0:
                # if prio is 0 -> not relevant (also if PRIORITY_IMPORTANCE_FACTOR prio is 1)
                concept_relevance += adjusted_priority * adjusted_weight

                pd_relevance += calculate_taxonomy_relevance_factor(prio_ilo.process_dimension,
                                                                   task_loa.process_dimension) * adjusted_weight
                kd_relevance += calculate_taxonomy_relevance_factor(prio_ilo.knowledge_dimension,
                                                                   task_loa.knowledge_dimension) * adjusted_weight

        if matching_concepts > 0:
            self.concept_relevance_reward = (concept_relevance / matching_concepts) * self.CONCEPT_RELEVANCE_FACTOR
            self.pd_relevance_reward = (pd_relevance / matching_concepts) * self.PD_RELEVANCE_FACTOR
            self.kd_relevance_reward = (kd_relevance / matching_concepts) * self.KD_RELEVANCE_FACTOR

        # Reward easier tasks for less competent students
        self.reward_difficulty_adjustment = 0
        if self.student.challenge_level >= selected_task.difficulty:
            self.reward_difficulty_adjustment = 1 * self.DIFFICULTY_FACTOR

        # Reward stress adjustment
        if 0.8 > self.student.current_stress > 0.5:
            # stress is in flow zone or adjustment was successful
            self.reward_stress = 1 * self.STRESS_FACTOR

        # Reward competency improvements
        matching_concepts = 0
        achieved_bonus = 0

        pd_improvement = 0
        kd_improvement = 0
        for c_after in self.student.ilo_progress:
            target_ilo = next((ilo for ilo in self.prio_list if ilo.concept.id == c_after.concept.id),
                              IntendedLearningOutcomeRelevance(c_after.concept, [-1] * 6, [-1] * 4, 0.1))
            c_before = next((cb for cb in student_before if cb.concept.id == c_after.concept.id), None)

            before_pd = c_before.process_dimension if c_before else [0] * len(target_ilo.process_dimension)
            before_kd = c_before.knowledge_dimension if c_before else [0] * len(target_ilo.knowledge_dimension)

            if c_before and (np.array_equal(before_pd, c_after.process_dimension) and
                              np.array_equal(before_kd, c_after.knowledge_dimension)):
                # Skip if the student has not improved on the concept
                continue

            matching_concepts += 1

            adjusted_relevance_factor = 1 - (self.PRIORITY_IMPORTANCE_FACTOR * (1 - target_ilo.priority))

            pd_factor = calculate_progress_improvement_and_bonus(before_pd,
                                                                           c_after.process_dimension,
                                                                           target_ilo.process_dimension)

            kd_factor = calculate_progress_improvement_and_bonus(before_kd,
                                                                           c_after.knowledge_dimension,
                                                                           target_ilo.knowledge_dimension)

            pd_improvement += pd_factor * adjusted_relevance_factor
            kd_improvement += kd_factor * adjusted_relevance_factor

            previously_achieved = self.is_dimension_achieved(before_pd, target_ilo.process_dimension) and \
                                  self.is_dimension_achieved(before_kd, target_ilo.knowledge_dimension)

            currently_achieved = self.is_dimension_achieved(c_after.process_dimension, target_ilo.process_dimension) and \
                                 self.is_dimension_achieved(c_after.knowledge_dimension, target_ilo.knowledge_dimension)

            if currently_achieved:
                achieved_bonus += 1
            elif previously_achieved:
                achieved_bonus -= 1

        if matching_concepts > 0:
            self.reward_pd_improvement = (pd_improvement / matching_concepts) * self.PD_IMPROVEMENT_FACTOR
            self.reward_kd_improvement = (kd_improvement / matching_concepts) * self.KD_IMPROVEMENT_FACTOR

        if achieved_bonus != 0:
            self.reward_bonus = achieved_bonus * self.BONUS_FACTOR

        # check if all ilo are achieved
        #progress_map = {ip.concept.id: ip for ip in self.student.ilo_progress}

        done = False
        #for ilo in self.prio_list:
        #    ilo_progress = progress_map.get(ilo.concept.id)
        #    if ilo_progress is None:
        #        done = False
        #        break
        #    if not (self.is_dimension_achieved(ilo_progress.process_dimension, ilo.process_dimension) and
        #            self.is_dimension_achieved(ilo_progress.knowledge_dimension, ilo.knowledge_dimension)):
        #        done = False
        #        break

        #done_reward = 0
        # reward early termination
        #if done:
        #    remaining_steps = self.NUMBER_OF_RECOMMENDATIONS - len(self.actions_used)
        #    maximum_reward_per_step = (self.KD_IMPROVEMENT_FACTOR +
        #                               self.PD_IMPROVEMENT_FACTOR +
        #                               self.CONCEPT_RELEVANCE_FACTOR +
        ##                               self.PD_RELEVANCE_FACTOR +
        #                               self.KD_RELEVANCE_FACTOR +
        #                               self.BONUS_FACTOR +
        #                               self.DIFFICULTY_FACTOR +
        #                               self.STRESS_FACTOR)
            # reward perfect recommendations for remaining steps
            # add 100 for being best in cumulative reward
            #done_reward = remaining_steps * maximum_reward_per_step + (10 * remaining_steps)

        if len(self.actions_used) >= self.NUMBER_OF_RECOMMENDATIONS:
            # terminate if max number of recommendations is reached
            done = True
            #done_reward = -100

        self.reward_complete = (#done_reward +
                                self.reward_new +

                                self.reward_difficulty_adjustment +
                                self.reward_stress +

                                self.concept_relevance_reward +
                                self.pd_relevance_reward +
                                self.kd_relevance_reward +

                                self.reward_pd_improvement +
                                self.reward_kd_improvement +
                                self.reward_bonus
                                )

        return (
            self.obs,
            self.reward_complete,
            done,
            False,
            {
                "success_rate": rate,

                "reward_new": self.reward_new,
                "reward_difficulty": self.reward_difficulty_adjustment,
                "reward_stress": self.reward_stress,

                "concept_relevance_reward": self.concept_relevance_reward,
                "pd_relevance_reward": self.pd_relevance_reward,
                "kd_relevance_reward": self.kd_relevance_reward,

                "pd_improvement_reward": self.reward_pd_improvement,
                "kd_improvement_reward": self.reward_kd_improvement,
                "bonus_reward": self.reward_bonus
            }
        )

    @staticmethod
    def is_dimension_achieved(progress_dims, required_dims, threshold=0.9):
        return all(p >= threshold for p, r in zip(progress_dims, required_dims) if r != 0)

    def create_abstract_task_from_action(self, action) -> Task:
        difficulty = action['difficulty']
        p_slip = action['p_slip']
        p_guess = action['p_guess']

        concept_ids = self.valid_concept_combinations[action['concept_ids']]
        pd_mapping_indices = action['pd_mappings']
        kd_mapping_indices = action['kd_mappings']
        weights = action['weights']

        learning_objective_assignments = []
        for idx in range(len(concept_ids)):
            concept_id = concept_ids[idx]
            if concept_id >= len(self.all_concepts):
                continue

            concept = self.all_concepts[concept_id]

            pd_mapping_index = pd_mapping_indices[idx]
            kd_mapping_index = kd_mapping_indices[idx]
            weight = weights[idx]

            pd_mapping = PD_ARRAY_LIST[pd_mapping_index]
            kd_mapping = KD_ARRAY_LIST[kd_mapping_index]

            loa = LearningObjectiveAssignment(
                concept=concept,
                process_dimension=pd_mapping,
                knowledge_dimension=kd_mapping,
                weight=float(weight)
            )
            learning_objective_assignments.append(loa)

        task = Task(
            task_id=0,
            name="abstract_task",
            p_slip=float(p_slip),
            p_guess=float(p_guess),
            link="optimales-lernobjekt.de",
            difficulty=float(difficulty),
            learning_objective_assignments=learning_objective_assignments
        )

        return task

    @staticmethod
    def mapping_similarity(action_mapping, assignment_dimension):
        matches = sum(1 for a, b in zip(action_mapping, assignment_dimension) if a == b)
        similarity = matches / len(action_mapping)
        return similarity

    def compute_similarity(self, action, task):
        action_difficulty = action['difficulty']
        action_p_slip = action['p_slip']
        action_p_guess = action['p_guess']
        action_concept_ids = self.valid_concept_combinations[action['concept_ids']]
        action_pd_mapping_indices = action['pd_mappings']
        action_kd_mapping_indices = action['kd_mappings']
        action_weights = action['weights']

        weight_difficulty = 0.6
        weight_pslip = 0.2
        weight_pguess = 0.2

        similarity_difficulty = 1 - abs(action_difficulty - task.difficulty)
        similarity_p_slip = 1 - abs(action_p_slip - task.p_slip)
        similarity_p_guess = 1 - abs(action_p_guess - task.p_guess)

        total_similarity = (
                similarity_difficulty * weight_difficulty +
                similarity_p_slip * weight_pslip +
                similarity_p_guess * weight_pguess
        )
        weight_total = weight_difficulty + weight_pslip + weight_pguess

        any_concept_found = False
        for idx in range(len(action_concept_ids)):
            concept_id = action_concept_ids[idx]

            if concept_id >= len(self.all_concepts):
                continue

            action_pd_index = action_pd_mapping_indices[idx]
            action_kd_index = action_kd_mapping_indices[idx]
            action_weight = action_weights[idx]

            action_pd_mapping = PD_ARRAY_LIST[action_pd_index]
            action_kd_mapping = KD_ARRAY_LIST[action_kd_index]

            concept_found = False
            for assignment in task.learning_objective_assignments:
                if assignment.concept.id == self.all_concepts[concept_id].id:
                    concept_found = True
                    any_concept_found = True

                    pd_similarity = self.mapping_similarity(action_pd_mapping, assignment.process_dimension)
                    kd_similarity = self.mapping_similarity(action_kd_mapping, assignment.knowledge_dimension)
                    weight_similarity = 1 - abs(action_weight - assignment.weight)

                    total_similarity += (pd_similarity + kd_similarity + weight_similarity)
                    weight_total += 3
                    break
            if not concept_found:
                # If the concept is not in the task, penalize the action and scale it by the weight
                weight_total += 3 * action_weight
        if not any_concept_found:
            return 0.0

        normalized_similarity = total_similarity / weight_total

        return normalized_similarity

    def find_most_similar_task(self, action):
        best_similarity = -1
        best_task = None
        for task in self.all_tasks:
            similarity = self.compute_similarity(action, task)
            if similarity > best_similarity:
                best_similarity = similarity
                best_task = task
        return best_task, best_similarity


def calculate_taxonomy_relevance_factor(prio_dimension, task_dimension):
    relevance_factors = [a * b for a, b in zip(prio_dimension,
                                               task_dimension)]

    helper = []
    relevance = 0
    for i, (prio, task) in enumerate(zip(prio_dimension, task_dimension)):
        if prio == 0 and task == 0:
            continue
        comparison = (prio > task) - (prio < task)
        helper.append(comparison)

        if comparison == 0:
            relevance += relevance_factors[i]
        elif comparison > 0:
            relevance += 0.5
        else:
            relevance -= 1

    return max(0.0, relevance / len(helper)) if helper else 0.0


def calculate_progress_improvement_and_bonus(dim_before, dim_after, mapping):
    diff = 0
    skip_counter = 0
    for i, (a, b) in enumerate(zip(dim_before, dim_after)):
        if mapping[i] == 0 or a == b:
            skip_counter += 1
            continue
        diff += max(0, b - a)

    factor = (diff / (len(dim_before) - skip_counter)) if skip_counter != len(dim_before) else 0
    return factor

from enum import Enum
from typing import Union, List

import numpy as np
from pydantic import BaseModel


class PPOHyperparamBounds(BaseModel):
    lr: List[float] = [1e-5, 1e-3]
    lambda_param: List[float] = [0.9, 1.0]
    clip_param: List[float] = [0.1, 0.5]
    num_sgd_iter: List[int] = [1, 30]
    sgd_minibatch_size: List[int] = [300, 800]
    train_batch_size: List[int] = [1000, 4000]


class DQNHyperparamBounds(BaseModel):
    adam_epsilon: List[float] = [1e-8, 1e-4]
    v_min: List[float] = [-10.0, 0.0]
    v_max: List[float] = [10.0, 100.0]
    training_intensity: List[float] = [1.0, 1000.0]


class PPOPB2Config(BaseModel):
    time_attr: str = "time_total_s"
    perturbation_interval: int = 120
    quantile_fraction: float = 0.25
    hyperparam_bounds: PPOHyperparamBounds = PPOHyperparamBounds()


class DQNPB2Config(BaseModel):
    time_attr: str = "time_total_s"
    perturbation_interval: int = 120
    quantile_fraction: float = 0.25
    hyperparam_bounds: DQNHyperparamBounds = DQNHyperparamBounds()


class PPOTrainParams(BaseModel):
    entropy_coeff: float = 0.1
    vf_loss_coeff: float = 0.5
    vf_clip_param: float = 10.0
    lambda_param: float = 0.95
    clip_param: float = 0.2
    lr: float = 1e-4
    num_sgd_iter: Union[List[int], int] = [10, 20]
    sgd_minibatch_size: Union[List[int], int] = [300, 500]
    train_batch_size: Union[List[int], int] = [1000, 1500, 2000]
    use_critic: bool = True
    use_gae: bool = True


class DQNTrainParams(BaseModel):
    target_network_update_freq: int = 500
    lr: float = 1e-4
    adam_epsilon: float = 1e-8
    num_steps_sampled_before_learning_starts: int = 1000
    tau: float = 0.001
    num_atoms: int = 1
    v_min: float = -10.0
    v_max: float = 10.0
    noisy: bool = False
    sigma0: float = 0.5
    dueling: bool = True
    double_q: bool = True
    n_step: Union[List[int], int] = 1


class Concept:
    def __init__(self, concept_id: int, name: str = None):
        self.id = concept_id
        if name is not None and not isinstance(name, str):
            raise ValueError("Name must be a string or None")
        self.name = name

    def __eq__(self, other):
        if isinstance(other, Concept):
            return self.id == other.id
        return False

    def to_dict(self):
        return {"id": self.id, "name": self.name}


class IntendedLearningOutcome:
    def __init__(self, concept: Concept, process_dimension: list[Union[int, float]],
                 knowledge_dimension: list[Union[int, float]]):
        self.concept = concept
        self.process_dimension = process_dimension
        self.knowledge_dimension = knowledge_dimension

    def to_dict(self):
        return {
            "concept": self.concept.to_dict(),
            "process_dimension": self.process_dimension,
            "knowledge_dimension": self.knowledge_dimension
        }


class IntendedLearningOutcomeProgress(IntendedLearningOutcome):
    def __init__(self, concept: Concept, process_dimension: list[float], knowledge_dimension: list[float]):
        super().__init__(concept, process_dimension, knowledge_dimension)

    def to_dict(self):
        ilo_dict = super().to_dict()
        return ilo_dict


class IntendedLearningOutcomeRelevance(IntendedLearningOutcome):
    def __init__(self, concept: Concept, process_dimension: list[int], knowledge_dimension: list[int], priority: float):
        super().__init__(concept, process_dimension, knowledge_dimension)
        self.priority = priority

    def to_dict(self):
        ilo_dict = super().to_dict()
        ilo_dict["priority"] = self.priority
        return ilo_dict


class JobStatus(str, Enum):
    pending = "pending"
    running = "running"
    completed = "completed"
    failed = "failed"


class LearningObjectiveAssignment(IntendedLearningOutcome):
    def __init__(self, concept: Concept, process_dimension: list[int], knowledge_dimension: list[int], weight: float):
        super().__init__(concept, process_dimension, knowledge_dimension)
        self.weight = weight

    def to_dict(self):
        ilo_dict = super().to_dict()
        ilo_dict["weight"] = self.weight
        return ilo_dict


class ReinforcementLearningAlgorithm(str, Enum):
    dqn = "dqn"
    ppo = "ppo"


class Task:
    def __init__(self,
                 task_id: int,
                 name: str,
                 p_slip: float,
                 p_guess: float,
                 link: str,
                 difficulty: float,
                 learning_objective_assignments: list[LearningObjectiveAssignment]):
        self.id = task_id
        self.name = name
        self.p_slip = p_slip
        self.p_guess = p_guess
        self.link = link
        self.difficulty = difficulty
        self.learning_objective_assignments = learning_objective_assignments

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "p_slip": self.p_slip,
            "p_guess": self.p_guess,
            "link": self.link,
            "difficulty": self.difficulty,
            "learning_objective_assignments": [loa.to_dict() for loa in self.learning_objective_assignments]
        }


class Student:
    def __init__(self,
                 ability: float,
                 challenge_level: float,
                 ilo_progress: list[IntendedLearningOutcomeProgress],
                 stress: float = 50):
        self.ability = ability
        self.challenge_level = challenge_level
        self.ilo_progress = ilo_progress
        self.solved_tasks: list[Task] = []
        self.current_stress = stress

    def estimate_recommendation(self, task: Task) -> float:
        # Add the task to the list of tasks the student has solved
        self.solved_tasks.append(task)

        # Initial success rate is based on the student's ability, with a slight shift towards the skill level
        success_rate = 0.5 + (self.ability - 0.5) * 0.2

        # Adjust the success rate based on the difficulty of the task
        success_rate -= (task.difficulty - 0.5) * 0.4

        # Calculate the average gaps between the task and the student
        pd_gaps = []
        kd_gaps = []
        for loa in task.learning_objective_assignments:
            matching_ilo_progress = next((ilo for ilo in self.ilo_progress if ilo.concept.id == loa.concept.id), None)

            if matching_ilo_progress is None:
                highest_kd_student_stage = 0
                highest_pd_student_stage = 0
            else:
                highest_kd_student_stage = max(
                    (i for i, val in enumerate(matching_ilo_progress.process_dimension) if val != 0),
                    default=0) + 1
                highest_pd_student_stage = max(
                    (i for i, val in enumerate(matching_ilo_progress.process_dimension) if val != 0),
                    default=0) + 1

            highest_pd_task_stage = max((i for i, val in enumerate(loa.process_dimension) if val != 0),
                                        default=0) + 1

            highest_kd_task_stage = max((i for i, val in enumerate(loa.process_dimension) if val != 0),
                                        default=0) + 1

            # Calculate the gaps
            pd_gap = highest_pd_task_stage - highest_pd_student_stage
            pd_gaps.append(pd_gap)

            kd_gap = highest_kd_task_stage - highest_kd_student_stage
            kd_gaps.append(kd_gap)

        # Adjust the success rate based on the student's expertise
        # Add offset (0.3) because if the student matches the required stage, the success rate should increase
        if len(pd_gaps) == 0:
            success_rate += 0.3
        else:
            success_rate -= ((np.mean(pd_gaps) / 6) * 0.4) - 0.3

        if len(kd_gaps) == 0:
            success_rate += 0.3
        else:
            success_rate -= ((np.mean(kd_gaps) / 4) * 0.4) - 0.3

        # Ensure the success rate is within the range of 0 to 1
        return min(1.0, max(0.0, success_rate))

    def update_progress_bkt(self, task: Task, success_rate: float) -> None:
        # Probability that the student correctly applies an unknown skill (has a lucky guess)
        p_guess = task.p_guess

        # Probability the student makes a mistake when applying a known skill
        p_slip = task.p_slip

        # Update the probability of skill mastery
        for loa in task.learning_objective_assignments:
            if not any(ilo_progress.concept.id == loa.concept.id for ilo_progress in self.ilo_progress):
                self.ilo_progress.append(IntendedLearningOutcomeProgress(loa.concept,
                                                                         [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                                                                         [0.0, 0.0, 0.0, 0.0]))

            for ilo_prog in self.ilo_progress:
                if loa.concept.id == ilo_prog.concept.id:
                    # Probability of the student demonstrating knowledge of the skill after an opportunity to apply it
                    p_transit = loa.weight * task.difficulty

                    ilo_prog.process_dimension = self.update_progress(success_rate,
                                                                          ilo_prog.process_dimension,
                                                                          loa.process_dimension,
                                                                          p_slip,
                                                                          p_guess,
                                                                          p_transit)

                    ilo_prog.knowledge_dimension = self.update_progress(success_rate,
                                                                            ilo_prog.knowledge_dimension,
                                                                            loa.knowledge_dimension,
                                                                            p_slip,
                                                                            p_guess,
                                                                            p_transit)
        return None

    @staticmethod
    def update_progress(success_rate,
                        progress: list[float],
                        mapping: list[int],
                        p_slip,
                        p_guess,
                        p_transit) -> list[float]:
        returning_list = progress
        epsilon = 1e-10

        for i, val in enumerate(progress):
            if mapping[i] == 0:
                continue

            # Probability of the student knowing the skill beforehand
            p_init = val

            if success_rate > 0.5:
                # Observation is correctly applying the skill
                p_next = (p_init * (1 - p_slip)) / (p_init * (1 - p_slip) + (1 - p_init) * p_guess + epsilon)
            else:
                # Observation is incorrectly applying the skill
                p_next = (p_init * p_slip) / (p_init * p_slip + (1 - p_init) * (1 - p_guess) + epsilon)

            # Update the student's competence
            returning_list[i] = min(1, max(0, p_next + (1 - p_next) * p_transit))

        return returning_list

    def adjust_stress(self, success_rate: float) -> None:
        if success_rate < 0.5:
            stress_increase = (0.5 - success_rate) * 1
            self.current_stress += stress_increase
        else:
            stress_decrease = (success_rate - 0.5) * 0.2
            self.current_stress -= stress_decrease

        self.current_stress = min(1, max(0, self.current_stress))
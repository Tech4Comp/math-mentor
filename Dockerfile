# syntax=docker/dockerfile:1

# Comments are provided throughout this file to help you get started.
# If you need more help, visit the Dockerfile reference guide at
# https://docs.docker.com/go/dockerfile-reference/

# Want to help us make this template better? Share your feedback here: https://forms.gle/ybq9Krt8jtBL3iCk7

ARG PYTHON_VERSION=3.10.12
FROM python:${PYTHON_VERSION}-slim as base

# Prevents Python from writing pyc files.
ENV PYTHONDONTWRITEBYTECODE=1

# Keeps Python from buffering stdout and stderr to avoid situations where
# the application crashes without emitting any logs due to buffering.
ENV PYTHONUNBUFFERED=1

# Set desired server's timezone.
ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

WORKDIR /app

# Create a non-privileged user that the app will run under.
# See https://docs.docker.com/go/dockerfile-user-best-practices/
# ARG HOST_UID=10001
# ARG HOST_GID=10001
# ENV HOST_UID=$HOST_UID
# ENV HOST_GID=$HOST_GID

# RUN groupadd -g ${HOST_GID} appuser

# RUN adduser \
#    --disabled-password \
#    --gecos "" \
#    --home "/home/appuser" \
#    --shell "/sbin/nologin" \
#    --uid "${HOST_UID}" \
#    --gid "${HOST_GID}" \
#    appuser

# Enable write access for home directory.
# ENV HOME="/home/appuser"
# RUN mkdir -p ${HOME}/.config/matplotlib
# RUN chown -R appuser:appuser ${HOME}

# Download dependencies as a separate step to take advantage of Docker's caching.
# Leverage a cache mount to /root/.cache/pip to speed up subsequent builds.
# Leverage a bind mount to requirements.txt to avoid having to copy them into
# into this layer.
RUN --mount=type=cache,target=/root/.cache/pip \
    --mount=type=bind,source=requirements.txt,target=requirements.txt \
    python -m pip install -U pip && \
    python -m pip install -r requirements.txt

# Switch to the non-privileged user to run the application.
# USER appuser

# Copy the source code into the container.
COPY . .

# Expose the ports that the application listens on.
EXPOSE 6006
EXPOSE 8000
EXPOSE 8265

# Run the application.
CMD ["sh", "-c", "ray start --head --dashboard-host 0.0.0.0 --disable-usage-stats && serve run api:app"]

import json
import os
import weakref
from datetime import datetime
from shutil import rmtree
from typing import Annotated, Dict, List
from uuid import uuid4, UUID

import ray
import requests
from cachetools import TTLCache
from fastapi import FastAPI, BackgroundTasks, Depends, HTTPException, Query, Response
from fastapi.middleware.cors import CORSMiddleware
from pydantic import constr, JsonValue, PositiveFloat, PositiveInt
from ray import serve

from auth import ensure_authorized
from classes import ReinforcementLearningAlgorithm, Student, PPOTrainParams, DQNTrainParams, \
    PPOPB2Config, DQNPB2Config, IntendedLearningOutcomeRelevance
from environment import RecommendationEnvironment
from handler.db_handler import DBHandler
from handler.influx_handler import InfluxDBHandler
from handler.job_info_handler import JobInfoHandler
from handler.rl_handler import RLHandler, IterationCallback

CHECKPOINT_DIR = "checkpoints/best"

app = FastAPI(root_path="/api/v1")

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

cache = TTLCache(maxsize=10, ttl=3600)
ray.init(address="auto", namespace="math-mentor")
serve.start(detached=True, http_options={"host": "0.0.0.0"})


@serve.deployment(ray_actor_options={"num_gpus": PositiveInt(os.environ["NUM_GPUS"])})
@serve.ingress(app)
class MathMentor:
    def __init__(self):
        # Connecting to the database.
        self.db_handler = DBHandler(
            host=os.environ["DB_HOST"],
            database=os.environ["DB_NAME"],
            user=os.environ["DB_USER"],
            password=os.environ["DB_PASSWORD"]
        )

        self.db_handler.connect()

        self.influx_handler = InfluxDBHandler(
            url=os.environ["INFLUX_URL"],
            token=os.environ["INFLUX_TOKEN"],
            org=os.environ["INFLUX_ORG"],
            bucket=os.environ["INFLUX_BUCKET"]
        )

        self.influx_handler.connect()

        # Automatically close connection on garbage collection.
        weakref.finalize(self.db_handler, self.db_handler.close)
        weakref.finalize(self.influx_handler, self.influx_handler.close)

        # Initializing RL handler.
        self.rl_handler = RLHandler(PositiveInt(os.environ["NUM_WORKERS"]))

        # Initializing handler that keeps track of submitted jobs.
        self.job_info_handler = JobInfoHandler()

        # If defined, load last used model after restart.
        last_used_path = os.path.join(CHECKPOINT_DIR, "last.txt")
        
        if not os.path.exists(last_used_path): 
            return

        with open(last_used_path, "r") as file:
            model_name = file.read().strip()
            model_path = os.path.join(CHECKPOINT_DIR, model_name)
                
            if not os.path.exists(model_path): 
                return
            
            self.rl_handler.load_model_from_checkpoint(model_path)

            with open(os.path.join(model_path, "meta.json"), "r") as file:
                meta_data = json.load(file)
                self.rl_handler.set_environment_params(meta_data.get("environment_params", {}))

    @staticmethod
    def request_notification(
            subtitle: constr(min_length=1),
            message: constr(min_length=1)
    ):
        if int(os.environ["GOTIFY_ENABLED"]) == 0:
            return

        try:
            requests.post(os.environ["GOTIFY_MESSAGE_URL"], json={
                "title": f"Math Mentor | {subtitle}",
                "message": message,
                "priority": 5
            })
        except requests.exceptions.RequestException as e:
            print(f"Notification handling failed: {str(e)}")

    def train_model(
            self,
            job_id: UUID,
            algorithm: ReinforcementLearningAlgorithm,
            episode_reward_mean_threshold: PositiveFloat,
            max_iterations: PositiveInt,
            num_samples: List[PositiveInt],
            use_pb2: bool,
            environment_params: Dict[str, float],
            pb2_config: Dict[str, float],
            initial_train_params: Dict[str, float]
    ) -> None:
        try:
            self.job_info_handler.add(job_id, algorithm, episode_reward_mean_threshold, max_iterations)

            tasks = self.db_handler.fetch_tasks()
            concepts = self.db_handler.fetch_all_concepts()

            # Retrieve available training algorithms.
            training_algorithm = self.rl_handler.get_available_training_algorithms().get(algorithm)

            # Return early if desired algorithm is not available.
            if training_algorithm is None:
                raise Exception("Training algorithm not supported")

            # Start actual training.
            episode_reward_mean = training_algorithm(
                job_id,
                tasks,
                concepts,
                environment_params,
                use_pb2,
                pb2_config,
                initial_train_params,
                [IterationCallback(self.job_info_handler.get(job_id))],
                num_samples,
                episode_reward_mean_threshold,
                max_iterations
            )

            self.job_info_handler.finalize_success(job_id, episode_reward_mean)
            self.request_notification(
                "Training",
                f"Job {str(job_id)} ({algorithm.upper()}) completed with an episode reward mean of {episode_reward_mean:.2f} at {datetime.now().strftime('%I:%M %p')}."
            )
        except Exception as e:
            self.job_info_handler.finalize_error(job_id, str(e))
            self.request_notification(
                "Training",
                f"Job {str(job_id)} ({algorithm.upper()}) failed at {datetime.now().strftime('%I:%M %p')}. For more info, refer to the training status endpoint."
            )

    @app.post("/train/start/ppo", dependencies=[Depends(ensure_authorized)])
    async def start_training(
            self,
            background_tasks: BackgroundTasks,
            pb2_config: PPOPB2Config,
            initial_train_params: PPOTrainParams,
            episode_reward_mean_threshold: Annotated[
                PositiveFloat,
                Query(title="Episode reward mean stop criteria for training", ge=10, le=22000)
            ] = 95.0,
            max_iterations: Annotated[
                PositiveInt,
                Query(title="Maximum number of training iterations", ge=1, le=10000)
            ] = 100,
            num_samples: Annotated[
                PositiveInt,
                Query(title="Number of parallel trails", ge=1, le=20)
            ] = 4,
            use_pb2: Annotated[
                bool,
                Query(title="Use PB2 Scheduler")
            ] = True,
            use_cont_action_space: Annotated[
                bool,
                Query(title="Use continuous action space")
            ] = False,
            number_of_recommendations: Annotated[
                int,
                Query(title="Number of recommendations per environment", ge=0, le=100)
            ] = 1,
            max_concepts_per_action: Annotated[
                int,
                Query(title="Number of Concepts per Action", ge=0, le=100)
            ] = 1,
            weight_importance_factor: Annotated[
                float,
                Query(title="Influence of weight in relevance calculation", ge=0, le=1)
            ] = 1,
            priority_importance_factor: Annotated[
                float,
                Query(title="Influence of priority in improvement calculation", ge=0, le=1)
            ] = 1,
            concept_relevance_factor: Annotated[
                float,
                Query(title="Relevance factor for concept", ge=0, le=100)
            ] = 1,
            pd_relevance_factor: Annotated[
                float,
                Query(title="Relevance factor for process dimension", ge=0, le=100)
            ] = 1,
            kd_relevance_factor: Annotated[
                float,
                Query(title="Relevance factor for knowledge dimension", ge=0, le=100)
            ] = 1,
            pd_improvement_factor: Annotated[
                float,
                Query(title="Improvement factor for process dimension", ge=0, le=100)
            ] = 1,
            kd_improvement_factor: Annotated[
                float,
                Query(title="Improvement factor for knowledge dimension", ge=0, le=100)
            ] = 1,
            bonus_factor: Annotated[
                float,
                Query(title="Bonus factor for each bonus achieved", ge=0, le=100)
            ] = 1,
            stress_factor: Annotated[
                float,
                Query(title="Factor for rewarding stress adjustment", ge=0, le=100)
            ] = 1,
            difficulty_factor: Annotated[
                float,
                Query(title="Factor for rewarding easier tasks", ge=0, le=100)
            ] = 1,
            new_factor: Annotated[
                float,
                Query(title="Factor for rewarding new tasks", ge=0, le=100)
            ] = 1
    ) -> JsonValue:
        job_id = uuid4()

        # A loaded model would block the training process. Therefore, start unloading if necessary.
        if self.rl_handler.model is not None:
            self.rl_handler.unload_model()

        environment_params = {
            "use_cont_action_space": use_cont_action_space,
            "weight_importance_factor": weight_importance_factor,
            "priority_importance_factor": priority_importance_factor,
            "concept_relevance_factor": concept_relevance_factor,
            "pd_relevance_factor": pd_relevance_factor,
            "kd_relevance_factor": kd_relevance_factor,
            "pd_improvement_factor": pd_improvement_factor,
            "kd_improvement_factor": kd_improvement_factor,
            "bonus_factor": bonus_factor,
            "max_concepts_per_action": max_concepts_per_action,
            "stress_factor": stress_factor,
            "difficulty_factor": difficulty_factor,
            "new_factor": new_factor,
            "number_of_recommendations": number_of_recommendations
        }

        background_tasks.add_task(
            self.train_model,
            job_id,
            ReinforcementLearningAlgorithm.ppo,
            episode_reward_mean_threshold,
            max_iterations,
            num_samples,
            use_pb2,
            environment_params,
            pb2_config.model_dump(),
            initial_train_params.model_dump()
        )

        return {"job_id": str(job_id)}

    @app.post("/train/start/dqn", dependencies=[Depends(ensure_authorized)])
    async def start_training(
            self,
            background_tasks: BackgroundTasks,
            pb2_config: DQNPB2Config,
            initial_train_params: DQNTrainParams,
            episode_reward_mean_threshold: Annotated[
                PositiveFloat,
                Query(title="Episode reward mean stop criteria for training", ge=10, le=22000)
            ] = 95.0,
            max_iterations: Annotated[
                PositiveInt,
                Query(title="Maximum number of training iterations", ge=1, le=10000)
            ] = 100,
            num_samples: Annotated[
                PositiveInt,
                Query(title="Number of parallel trails", ge=1, le=20)
            ] = 4,
            use_pb2: Annotated[
                bool,
                Query(title="Use PB2 Scheduler")
            ] = True,
            number_of_recommendations: Annotated[
                int,
                Query(title="Number of recommendations per environment", ge=0, le=100)
            ] = 1,
            max_concepts_per_action: Annotated[
                int,
                Query(title="Number of Concepts per Action", ge=0, le=100)
            ] = 1,
            weight_importance_factor: Annotated[
                float,
                Query(title="Influence of weight in relevance calculation", ge=0, le=1)
            ] = 1,
            priority_importance_factor: Annotated[
                float,
                Query(title="Influence of priority in improvement calculation", ge=0, le=1)
            ] = 1,
            concept_relevance_factor: Annotated[
                float,
                Query(title="Relevance factor for concept", ge=0, le=100)
            ] = 10,
            pd_relevance_factor: Annotated[
                float,
                Query(title="Relevance factor for process dimension", ge=0, le=100)
            ] = 1,
            kd_relevance_factor: Annotated[
                float,
                Query(title="Relevance factor for knowledge dimension", ge=0, le=100)
            ] = 1,
            pd_improvement_factor: Annotated[
                float,
                Query(title="Improvement factor for process dimension", ge=0, le=100)
            ] = 1,
            kd_improvement_factor: Annotated[
                float,
                Query(title="Improvement factor for knowledge dimension", ge=0, le=100)
            ] = 1,
            bonus_factor: Annotated[
                float,
                Query(title="Bonus factor for each bonus achieved", ge=0, le=100)
            ] = 1,
            stress_factor: Annotated[
                float,
                Query(title="Factor for rewarding stress adjustment", ge=0, le=100)
            ] = 1,
            difficulty_factor: Annotated[
                float,
                Query(title="Factor for rewarding easier tasks", ge=0, le=100)
            ] = 1
    ) -> JsonValue:
        job_id = uuid4()

        # A loaded model would block the training process. Therefore, start unloading if necessary.
        if self.rl_handler.model is not None:
            self.rl_handler.unload_model()

        environment_params = {
            "weight_importance_factor": weight_importance_factor,
            "priority_importance_factor": priority_importance_factor,
            "concept_relevance_factor": concept_relevance_factor,
            "pd_relevance_factor": pd_relevance_factor,
            "kd_relevance_factor": kd_relevance_factor,
            "pd_improvement_factor": pd_improvement_factor,
            "kd_improvement_factor": kd_improvement_factor,
            "bonus_factor": bonus_factor,
            "max_concepts_per_action": max_concepts_per_action,
            "stress_factor": stress_factor,
            "difficulty_factor": difficulty_factor,
            "number_of_recommendations": number_of_recommendations
        }

        background_tasks.add_task(
            self.train_model,
            job_id,
            ReinforcementLearningAlgorithm.dqn,
            episode_reward_mean_threshold,
            max_iterations,
            num_samples,
            use_pb2,
            environment_params,
            pb2_config.model_dump(),
            initial_train_params.model_dump()
        )

        return {"job_id": str(job_id)}

    @app.get("/train/status/{job_id}", dependencies=[Depends(ensure_authorized)])
    async def get_training_status(self, job_id: UUID) -> JsonValue:
        return dict(sorted(self.job_info_handler.get(job_id).items()))

    @app.get("/models")
    async def list_models(self) -> JsonValue:
        try:
            def get_metadata(model: str) -> JsonValue:
                with open(os.path.join(CHECKPOINT_DIR, model, "meta.json")) as file:
                    return {
                        **{"model": model},
                        **{k: v for k, v in dict(json.load(file)).items() if
                           k in ["max_iterations", "train_batch_size"]}
                    }

            return [get_metadata(model) for model in os.listdir(CHECKPOINT_DIR) if model != "last.txt"]
        except FileNotFoundError:
            raise HTTPException(status_code=404, detail="Model directory not found")

    @app.delete("/models/delete/{model_name}", dependencies=[Depends(ensure_authorized)])
    async def delete_model(self, model_name: str) -> Dict[str, object]:
        model_path = os.path.join(CHECKPOINT_DIR, model_name)
        if os.path.exists(model_path):
            try:
                if self.rl_handler.loaded_model_path == model_path:
                    self.rl_handler.unload_model()

                last_used_path = os.path.join(CHECKPOINT_DIR, "last.txt")
                is_marked_for_delete = False

                if (os.path.exists(last_used_path)):
                    with open(last_used_path, "r") as file:
                        name = file.read().strip()
                        is_marked_for_delete = name == model_name

                    if is_marked_for_delete:
                        os.remove(last_used_path)

                rmtree(model_path)
                return {"message": f"Model {model_name} deleted successfully"}
            except Exception as e:
                raise HTTPException(status_code=500,
                                    detail=f"Failed to delete model: {str(e)}")
        else:
            raise HTTPException(status_code=404, detail="Model not found")

    @app.post("/models/load/{model_name}", dependencies=[Depends(ensure_authorized)])
    async def load_model(self, model_name: str) -> JsonValue:
        if self.rl_handler.model is not None:
            self.rl_handler.unload_model()

        model_path = os.path.join(CHECKPOINT_DIR, model_name)
        if os.path.exists(model_path):
            self.rl_handler.load_model_from_checkpoint(model_path)

            with open(os.path.join(model_path, "meta.json"), "r") as file:
                meta_data = json.load(file)
                self.rl_handler.set_environment_params(meta_data.get("environment_params", {}))

            with open(os.path.join(CHECKPOINT_DIR, "last.txt"), "w") as file:
                file.write(model_name)

            return {"message": f"Model {model_name} loaded successfully"}
        else:
            raise HTTPException(status_code=404, detail="Model not found")

    @app.get("/models/current")
    async def get_loaded_model(self) -> Dict[str, object]:
        if self.rl_handler.loaded_model_path is not None:
            return {
                "model": os.path.basename(self.rl_handler.loaded_model_path),
                "message": "Model is currently loaded",
                "environment_params": self.rl_handler.environment_params
            }
        else:
            return {
                "model": None,
                "message": "No model is currently loaded",
                "environment_params": None
            }

    def benchmark(self, job_id: UUID, episodes: PositiveInt, format_as: str):
        try:
            tasks = self.db_handler.fetch_tasks()
            concepts = self.db_handler.fetch_all_concepts()
            result = self.rl_handler.run_benchmark(tasks, concepts, episodes, format_as)
            cache[str(job_id)] = {"format": format_as, "result": result}
            self.request_notification(
                "Benchmark",
                f"Job {job_id} ({format_as.capitalize()}) completed at {datetime.now().strftime('%I:%M %p')}."
            )
        except Exception as e:
            cache[str(job_id)] = {"error": str(e)}
            self.request_notification(
                "Benchmark",
                f"Job {job_id} ({format_as.capitalize()}) failed at {datetime.now().strftime('%I:%M %p')}."
            )

    @app.post("/benchmark", dependencies=[Depends(ensure_authorized)])
    async def start_benchmark(
            self,
            background_tasks: BackgroundTasks,
            episodes: Annotated[int, Query(title="Number of training iterations", ge=1, le=1000)] = 100,
            format_as: Annotated[str, Query(title="Expected result format", enum=["image", "json"])] = "image"
    ) -> JsonValue:
        if self.rl_handler.model is None:
            return {"error": "No model loaded. Please load a model first."}

        job_id = uuid4()

        background_tasks.add_task(self.benchmark, job_id, episodes, format_as)

        return {"job_id": str(job_id)}

    @app.get("/benchmark/{job_id}", dependencies=[Depends(ensure_authorized)])
    async def get_benchmark_results(self, job_id: UUID):
        data = cache.get(str(job_id))

        if data is None:
            return {"error": "Benchmark result not available."}

        return data["result"] if data["format"] == "json" else Response(data["result"], media_type="image/png")

    @app.get("/concepts")
    async def get_concepts(self) -> JsonValue:
        try:
            concepts = self.db_handler.fetch_all_concepts()
            concepts_dicts = [concept.to_dict() for concept in concepts]
            return concepts_dicts
        except Exception:
            raise HTTPException(status_code=500, detail="Error fetching concepts")

    @app.post("/recommendation")
    async def get_recommendation(
            self,
            email: Annotated[str, Query(title="Mail address of user")] = "",
            course_id: Annotated[int, Query(title="Course id for recommendation")] = 0,
            challenge_level: Annotated[float, Query(title="Desired challenge level")] = 0,
            interested_concepts: Annotated[List[int], Query(title="List of interested concept IDs")] = None,
    ) -> JsonValue:
        if self.rl_handler.model is None:
            raise HTTPException(status_code=503, detail="Currently no model loaded.")

        tasks = self.db_handler.fetch_tasks()
        concepts = self.db_handler.fetch_all_concepts()

        current_stress = self.influx_handler.fetch_level_by_email(email)
        if current_stress is None:
            # If no stress level is available, set to 0.5
            current_stress = 0.5
        else:
            # Normalize stress level to [0, 1]
            current_stress = current_stress / 100

        if email != "":
            student_id = self.db_handler.retrieve_or_register_person_uuid(email)
            if student_id is None:
                raise HTTPException(status_code=500, detail="Retrieving or creating person failed.")
            student = self.db_handler.fetch_student_progress(student_id)
            student.solved_tasks = self.db_handler.fetch_completed_tasks(student_id)
            student.current_stress = current_stress
        else:
            student = Student(1.0, 1.0, [], current_stress)
        prio_list = self.db_handler.fetch_currently_relevant_concepts(course_id)

        if interested_concepts:
            for concept_id in interested_concepts:
                existing_prio = next((prio for prio in prio_list if prio.concept.id == concept_id), None)
                if existing_prio:
                    # already in Prio -> just adapt prio
                    existing_prio.priority = 1.0
                else:
                    concept = next((c for c in concepts if c.id == concept_id), None)
                    if concept:
                        new_prio = IntendedLearningOutcomeRelevance(
                            concept,
                            process_dimension=[1, 1, 1, 0, 0, 0],
                            knowledge_dimension=[1, 1, 0, 0],
                            priority=1.0
                        )
                        prio_list.append(new_prio)

        student.challenge_level = challenge_level

        env = RecommendationEnvironment({
            "all_tasks": tasks,
            "chooseRandom": False,
            "all_concepts": concepts,
            "student": student,
            "prio_list": prio_list,
            **self.rl_handler.environment_params
        })

        task, similarity, ideal_task = self.rl_handler.recommend_task(env)
        lo_id = self.db_handler.fetch_learning_object_id_by_task_id(task.id)
        result = {
            "learning_object_id": lo_id,
            "recommended_task": task.to_dict()
        }

        if env.use_cont_action_space:
            result.update({
                "similarity": similarity,
                "ideal_action": ideal_task.to_dict()
            })

        return result

    @app.post("/recommendation/result")
    async def set_result(
            self,
            email: Annotated[str, Query(title="Mail address of user")] = "",
            object_id: Annotated[int, Query(title="Learning Object Id of solved element")] = 0,
            duration: Annotated[int, Query(title="Duration needed to solve element")] = 0,
            was_successful: Annotated[bool, Query(title="Element was solved successful")] = True
    ) -> JsonValue:
        if email == "":
            raise HTTPException(status_code=400, detail="No mail address provided.")

        student_id = self.db_handler.retrieve_or_register_person_uuid(email)
        if student_id is None:
            raise HTTPException(status_code=500, detail="Retrieving oder creating person failed.")
        student = self.db_handler.fetch_student_progress(student_id)

        task = self.db_handler.fetch_task_by_learning_object_id(object_id)
        if task is None:
            raise HTTPException(status_code=500, detail="Error retrieving task from learning object id.")

        sr = 1.0 if was_successful else 0.0
        student.update_progress_bkt(task, sr)

        updated_concepts = {}
        for ta in task.learning_objective_assignments:
            for ilo in student.ilo_progress:
                if ilo.concept.id == ta.concept.id:
                    updated_concepts[ta.concept.id] = (ilo.process_dimension, ilo.knowledge_dimension)

        self.db_handler.insert_progress_and_concept_progress(student_id,
                                                             object_id,
                                                             was_successful,
                                                             duration,
                                                             updated_concepts)

        return {"message": "Result successfully stored."}


app = MathMentor.bind()

import json
import uuid
from datetime import datetime
import psycopg2
from classes import Task, LearningObjectiveAssignment, IntendedLearningOutcome, Student, \
    IntendedLearningOutcomeProgress, IntendedLearningOutcomeRelevance, Concept


class DBHandler:
    """
    Handles database operations.

    Attributes:
        connection (psycopg2.connection): The connection object to the database.
    """
    def __init__(self, host, database, user, password):
        """
        Initializes a new instance of the DBHandler class.

        Parameters:
            host (str): The hostname of the database server.
            database (str): The name of the database to connect to.
            user (str): The username for accessing the database.
            password (str): The password for accessing the database.
        """
        self.host = host
        self.database = database
        self.user = user
        self.password = password
        self.connection = None

    def connect(self):
        """
        Establishes a connection to the configured database.

        This method attempts to connect to the database using the stored credentials.
        Upon successful connection, `self.connection` is set and a confirmation is printed.
        """
        self.connection = psycopg2.connect(host=self.host, database=self.database, user=self.user,
                                           password=self.password)
        print("Database connection established")

    def close(self):
        """
        Closes the active database connection.

        This method checks if there is an active connection (`self.connection` is not None).
        If a connection exists, it is closed and a confirmation is printed.
        """
        if self.connection:
            self.connection.close()
            print("Database connection closed")

    def fetch_tasks(self) -> list[Task]:
        """
        Fetches tasks and their related learning objective assignments from the database.

        Returns:
            list: A list of Task instances.
        """
        if self.connection is None:
            print("No database connection. Call connect() first.")
            return list()

        cursor = self.connection.cursor()
        try:
            # Query to fetch task details along with related learning objectives
            cursor.execute("""
                SELECT t.TaskId, lo.Name, t.Link, t.Difficulty, t.SlipProbability, t.GuessProbability, 
                       loa.ConceptId, c.Name as ConceptName, loa.Weight, loa.ProcessDimension, loa.KnowledgeDimension
                FROM Tasks t
                INNER JOIN LearningObjects lo ON t.LearningObjectId = lo.LearningObjectId
                LEFT JOIN LearningObjectiveAssignment loa ON lo.LearningObjectId = loa.LearningObjectId
                LEFT JOIN Concepts c ON loa.ConceptId = c.ConceptId
                ORDER BY t.TaskId
            """)
            tasks_data = cursor.fetchall()

            # Organize data into Task instances
            tasks = {}
            for task_id, name, link, difficulty, p_slip, p_guess, concept_id, concept_name, weight, pd, kd in tasks_data:
                if task_id not in tasks:
                    tasks[task_id] = Task(task_id, name, float(p_slip), float(p_guess), link, float(difficulty), [])
                if concept_id:  # Ensure there is a related concept
                    learning_objective_assignment = LearningObjectiveAssignment(
                        Concept(concept_id, concept_name),
                        pd,
                        kd,
                        weight
                    )
                    tasks[task_id].learning_objective_assignments.append(learning_objective_assignment)
            return list(tasks.values())
        finally:
            cursor.close()

    def retrieve_or_register_person_uuid(self, email) -> str:
        """
        Adds a new person and a corresponding entry in personmapping if no person with the given email exists.

        Parameters:
            email (str): The email of the person.

        Returns:
            bool: True if the person was added, False if the person already exists.
        """
        if self.connection is None:
            print("No database connection. Call connect() first.")
            return None

        cursor = self.connection.cursor()
        try:
            cursor.execute(
                "SELECT id, uuid FROM person JOIN personmapping ON person.id = personmapping.personid WHERE email = %s",
                (email,))
            result = cursor.fetchone()
            if result:
                return result[1]

            cursor.execute("INSERT INTO person (email) VALUES (%s) RETURNING id", (email,))
            person_id = cursor.fetchone()[0]
            self.connection.commit()

            uuid_str = str(uuid.uuid4())
            cursor.execute("INSERT INTO personmapping (uuid, personid) VALUES (%s, %s)", (uuid_str, person_id))
            self.connection.commit()

            return uuid_str
        except psycopg2.Error as e:
            if self.connection:
                self.connection.rollback()
            print(f"Failed to add person or personmapping entry: {e}")
            return None
        finally:
            cursor.close()

    def fetch_student_progress(self, student_id) -> Student:
        """
        Fetches the latest progress for each concept for a given student.

        Parameters:
            student_id (str): The identifier of the student.

        Returns:
            dict: A dictionary with concept ids as keys and the latest progress data as values.
        """
        if self.connection is None:
            print("No database connection. Call connect() first.")
            return {}

        cursor = self.connection.cursor()
        try:
            # Query to fetch the latest progress for each concept for a given student
            cursor.execute("""
                WITH LatestProgress AS (
                    SELECT cp.ConceptId, MAX(p.Timestamp) as MaxTimestamp
                    FROM Progress p
                    JOIN ConceptProgress cp ON p.ProgressId = cp.ProgressId
                    WHERE p.StudentId = %s
                    GROUP BY cp.ConceptId
                )
                SELECT cp.ConceptId, c.Name, cp.PDProgress, cp.KDProgress
                FROM ConceptProgress cp
                JOIN Progress p ON cp.ProgressId = p.ProgressId
                JOIN LatestProgress lp ON cp.ConceptId = lp.ConceptId AND p.Timestamp = lp.MaxTimestamp
                JOIN Concepts c ON cp.ConceptId = c.ConceptId
                WHERE p.StudentId = %s
            """, (student_id, student_id))
            progress_data = cursor.fetchall()

            # Organize the latest progress data into a dictionary
            all_progress = []
            for concept_id, concept_name, pd_progress, kd_progress in progress_data:
                progress = IntendedLearningOutcomeProgress(Concept(concept_id, concept_name), pd_progress, kd_progress)
                all_progress.append(progress)

            return Student(1.0, 1.0, all_progress)
        finally:
            cursor.close()

    def fetch_completed_tasks(self, student_id) -> list[Task]:
        """
        Fetches tasks that a student has successfully completed

        Parameters:
            student_id (str): The identifier of the student.

        Returns:
            list: A list of Task instances
        """
        if self.connection is None:
            print("No database connection. Call connect() first.")
            return []

        cursor = self.connection.cursor()
        try:
            # Query to fetch all tasks and their related learning objectives that the student has successfully completed
            cursor.execute("""
                SELECT t.TaskId, lo.Name, t.Link, t.Difficulty, t.SlipProbability, t.GuessProbability, 
                       loa.ConceptId, c.Name as ConceptName, loa.Weight, loa.ProcessDimension, loa.KnowledgeDimension
                FROM Tasks t
                JOIN Progress p ON t.LearningObjectId = p.LearningObjectId
                JOIN LearningObjects lo ON t.LearningObjectId = lo.LearningObjectId
                LEFT JOIN LearningObjectiveAssignment loa ON lo.LearningObjectId = loa.LearningObjectId
                LEFT JOIN Concepts c ON loa.ConceptId = c.ConceptId
                WHERE p.StudentId = %s 
            """, (student_id,))
            tasks_data = cursor.fetchall()

            # Organize data into Task instances, including learning objective assignments
            tasks = {}
            for task_id, name, link, difficulty, p_slip, p_guess, concept_id, concept_name, weight, pd, kd in tasks_data:
                if task_id not in tasks:
                    tasks[task_id] = Task(task_id, name, float(p_slip), float(p_guess), link, float(difficulty), [])
                if concept_id:  # Ensure there is a related concept
                    learning_objective_assignment = LearningObjectiveAssignment(
                        Concept(concept_id, concept_name),
                        pd,
                        kd,
                        weight
                    )
                    tasks[task_id].learning_objective_assignments.append(learning_objective_assignment)
            return list(tasks.values())
        finally:
            cursor.close()

    def fetch_learning_object_id_by_task_id(self, task_id) -> int:
        """
        Retrieves the LearningObjectId associated with a given TaskId.

        Parameters:
            task_id (int): The identifier of the task.

        Returns:
            int: The LearningObjectId if found, None otherwise.
        """
        if self.connection is None:
            print("No database connection. Call connect() first.")
            return None

        cursor = self.connection.cursor()
        try:
            # Query to fetch the LearningObjectId for the specified task
            cursor.execute("""
                SELECT LearningObjectId
                FROM Tasks
                WHERE TaskId = %s
            """, (task_id,))
            result = cursor.fetchone()
            if result:
                return result[0]
            else:
                print("No LearningObjectId found for the specified TaskId.")
                return None
        except psycopg2.Error as e:
            print(f"Failed to fetch LearningObjectId: {e}")
            return None
        finally:
            cursor.close()

    def fetch_task_by_learning_object_id(self, learning_object_id) -> Task:
        """
        Fetches a task and its related learning objective assignments by a given LearningObjectId.

        Parameters:
            learning_object_id (int): The identifier of the learning object.

        Returns:
            Task: An instance of Task if found, None otherwise.
        """
        if self.connection is None:
            print("No database connection. Call connect() first.")
            return None

        cursor = self.connection.cursor()
        try:
            # Query to fetch task details along with related learning objectives
            cursor.execute("""
                SELECT t.TaskId, lo.Name, t.Link, t.Difficulty, t.SlipProbability, t.GuessProbability,
                       loa.ConceptId, c.Name as ConceptName, loa.Weight, loa.ProcessDimension, loa.KnowledgeDimension
                FROM Tasks t
                INNER JOIN LearningObjects lo ON t.LearningObjectId = lo.LearningObjectId
                LEFT JOIN LearningObjectiveAssignment loa ON lo.LearningObjectId = loa.LearningObjectId
                LEFT JOIN Concepts c ON loa.ConceptId = c.ConceptId
                WHERE t.LearningObjectId = %s
            """, (learning_object_id,))
            tasks_data = cursor.fetchall()

            if not tasks_data:
                print("No task found with the specified LearningObjectId.")
                return None

            # Organize data into Task instance
            task = None
            for task_id, name, link, difficulty, p_slip, p_guess, concept_id, concept_name, weight, pd, kd in tasks_data:
                if task is None:
                    task = Task(task_id, name, float(p_slip), float(p_guess), link, float(difficulty), [])
                if concept_id:  # Ensure there is a related concept
                    learning_objective_assignment = LearningObjectiveAssignment(
                        Concept(concept_id, concept_name),
                        pd,
                        kd,
                        weight
                    )
                    task.learning_objective_assignments.append(learning_objective_assignment)
            return task
        finally:
            cursor.close()

    def fetch_currently_relevant_concepts(self, course_id) -> list[IntendedLearningOutcomeRelevance]:
        """
        Fetches concepts that are currently relevant for a given course, based on the current date and time.

        Parameters:
            course_id (int): The identifier of the course.

        Returns:
            list: A list of dictionaries, each containing concept ID, priority, process dimension, and knowledge dimension.
        """
        if self.connection is None:
            print("No database connection. Call connect() first.")
            return []

        current_time = datetime.now()
        cursor = self.connection.cursor()
        try:
            # Query to find relevant concepts for the course based on the current time
            cursor.execute("""
                SELECT c.ConceptId, c.Name, cr.Priority, cr.ProcessDimension, cr.KnowledgeDimension
                FROM ConceptRelevance cr
                JOIN Concepts c ON cr.ConceptId = c.ConceptId
                WHERE cr.CourseId = %s AND %s BETWEEN cr.StartTime AND cr.EndTime
            """, (course_id, current_time))
            concepts_data = cursor.fetchall()

            # Format the fetched data into a list of dictionaries
            prio_list = []
            for concept_id, name, priority, pd, kd in concepts_data:
                relevance = IntendedLearningOutcomeRelevance(Concept(concept_id, name), pd, kd, priority)
                prio_list.append(relevance)

            return prio_list
        finally:
            cursor.close()

    def fetch_all_concepts(self) -> list[Concept]:
        """
        Retrieves all concepts from the database.

        Returns:
            list of dicts: A list containing dictionary objects, each representing a concept with its ID and name.
        """
        if self.connection is None:
            print("No database connection. Call connect() first.")
            return []

        cursor = self.connection.cursor()
        try:
            cursor.execute("SELECT ConceptId, Name FROM Concepts ORDER BY ConceptId ASC")
            concepts = cursor.fetchall()

            return [Concept(concept[0], concept[1]) for concept in concepts]
        except psycopg2.Error as e:
            print("Failed to fetch concepts:", e)
            return []
        finally:
            cursor.close()

    def insert_progress_and_concept_progress(self,
                                             student_id,
                                             learning_object_id,
                                             was_successful,
                                             duration,
                                             concept_progress_dict):
        """
        Inserts the progress of a student and updates the concept progress based on the learning object.

        Parameters:
            student_id (str): The identifier of the student.
            learning_object_id (int): The identifier of the learning object.
            was_successful (bool): Indicates whether the student was successful in completing the learning object.
            duration (int): The duration of the learning object completion.
            concept_progress_dict (dict): A dictionary containing the concept progress data.
        """
        if self.connection is None:
            print("No database connection. Call connect() first.")
            return

        cursor = self.connection.cursor()
        try:
            # Insert the progress of the student
            cursor.execute("""
                INSERT INTO Progress (StudentId, LearningObjectId, Duration, Successful)
                VALUES (%s, %s, %s, %s) RETURNING ProgressId;
            """, (student_id, learning_object_id, duration, was_successful))
            progress_id = cursor.fetchone()[0]

            # Update the concept progress based on the learning object
            for concept_id, (pd_progress, kd_progress) in concept_progress_dict.items():
                cursor.execute("""
                    INSERT INTO ConceptProgress (ProgressId, ConceptId, PDProgress, KDProgress)
                    VALUES (%s, %s, %s, %s);
                """, (progress_id, concept_id, json.dumps(pd_progress), json.dumps(kd_progress)))

            self.connection.commit()
        except psycopg2.Error as e:
            if self.connection:
                self.connection.rollback()
            print(f"Failed to insert progress and update concept progress: {e}")
        finally:
            cursor.close()

###############################################################################
# EXAMPLE USAGE
###############################################################################
#db_handler = DBHandler(host="localhost", database="admin", user="admin", password="admin")
#db_handler.connect()

#concepts = db_handler.fetch_all_concepts()
#print("All Concepts:", concepts)

#course_id = 1
#current_relevant_concepts = db_handler.fetch_currently_relevant_concepts(course_id)
#for concept in current_relevant_concepts:
#    print(concept)

#student_id = '1CR5EKA2gia3/ojqgWKnL87JLGwJB12+oTWHBkzxgYmzTLPqZeNPLriWN+LrUCTZ6V7VUlpz9d/aOeZcrFOFJg=='
#student_progress = db_handler.fetch_student_progress(student_id)
#print(student_progress)

#tasks = db_handler.fetch_tasks()
#for task in tasks:
#    print(task.name, task.difficulty, [(loa.concept_id, loa.weight, loa.process_dimension) for loa in task.learning_objective_assignments])


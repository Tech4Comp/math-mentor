import io
import json
import os
import pprint
from copy import deepcopy
from datetime import datetime
from shutil import rmtree
from typing import Callable, Dict, List, Optional, Union
from uuid import UUID

import matplotlib.pyplot as plt
import numpy as np
import psutil
import torch
from pydantic import PositiveFloat, PositiveInt
from ray import train, tune
from ray.rllib.algorithms import Algorithm
from ray.tune.experiment import Trial
from ray.tune.schedulers.pb2 import PB2

from classes import Concept, ReinforcementLearningAlgorithm, Task, JobStatus
from environment import RecommendationEnvironment


def short_trial_dirname_creator(trial):
    return f"{trial.trainable_name}_{trial.trial_id[:8]}"


class IterationCallback(tune.Callback):
    """
    Callback to be invoked on each training iteration.
    """

    def __init__(self, info: Dict[str, object]):
        super().__init__()
        self.info = info

    def on_trial_result(self, iteration: int, trials: List[Trial], trial: Trial, result: Dict, **info):
        self.info.update({
            "iteration": int(result["training_iteration"]),
            "status": JobStatus.running,
            "train_batch_size": int(result["config"]["train_batch_size"]),
            "updated_at": datetime.now().strftime("%Y-%m-%dT%H:%M:%S%z")
        })


class RLHandler:
    """
    Handles operations related to Reinforcement Learning models using Ray.
    """

    def __init__(self, num_workers: PositiveInt = 4):
        """
        Initializes the RLHandler by setting up a connection with Ray.
        """
        self.model = None
        self.loaded_model_path = None

        self.environment_params = None

        # GPU related settings.
        self.is_gpu_available = torch.cuda.is_available()
        self.gpu_count = PositiveInt(os.environ["NUM_GPUS"]) if self.is_gpu_available else 0

        # Number of logical CPU cores. Note that one logical core must be reserved for coordination.
        self.cpu_count = psutil.cpu_count() - 1

        # Available memory in bytes. A small fraction keeps reserved for system purposes.
        self.memory = int(psutil.virtual_memory().available * 0.8)

        # Unlike with GPUs, each worker has to have at least one logical CPU core.
        # If the number of available cores is smaller, adjust the number of workers.
        self.num_workers = min(num_workers, self.cpu_count)

    def get_available_training_algorithms(self) \
            -> Dict[ReinforcementLearningAlgorithm, Callable[
                [UUID, List[Task], List[Concept], Dict[str, float], bool, Dict[str, float], Dict[str, float],
                 Optional[List[tune.Callback]],
                 Optional[List[PositiveInt]], PositiveFloat, PositiveInt], float]]:
        """
        Returns available training algorithms.
        """
        return {
            ReinforcementLearningAlgorithm.dqn: self.__train_dqn,
            ReinforcementLearningAlgorithm.ppo: self.__train_ppo
        }

    def set_environment_params(self, params: Dict[str, float]) -> None:
        self.environment_params = params

    def __train_ppo(
            self,
            job_id: UUID,
            tasks: List[Task],
            concepts: List[Concept],
            environment_params: Dict[str, float],
            use_pb2: bool,
            pb2_config: Dict[str, float],
            initial_train_params: Dict[str, float],
            callbacks: Optional[List[tune.Callback]],
            num_samples: PositiveInt,
            episode_reward_mean_threshold: PositiveFloat = 95.0,
            max_iterations: PositiveInt = 100
    ) -> float:
        """
        Placeholder method for training a model using the Proximal Policy Optimization algorithm.
        """
        print("Training PPO model...")

        # Randomly choose from list if the parameter is a list
        for key, value in initial_train_params.items():
            if isinstance(value, list):
                initial_train_params[key] = tune.choice(value)

        # Ensure lambda_param is renamed to lambda
        if "lambda_param" in initial_train_params:
            initial_train_params["lambda"] = initial_train_params.pop("lambda_param")

        if "lambda_param" in pb2_config["hyperparam_bounds"]:
            pb2_config["hyperparam_bounds"]["lambda"] = pb2_config["hyperparam_bounds"].pop("lambda_param")

        pb2 = PB2(**pb2_config)

        tuner = tune.Tuner(
            "PPO",
            tune_config=tune.TuneConfig(
                metric="env_runners/episode_reward_mean",
                mode="max",
                num_samples=num_samples,
                scheduler=pb2 if use_pb2 else None,
                trial_dirname_creator=short_trial_dirname_creator
            ),
            param_space={
                "env": RecommendationEnvironment,
                "env_config": {
                    "all_concepts": concepts,
                    "all_tasks": tasks,
                    "chooseRandom": True,
                    **environment_params
                },
                "num_gpus_per_env_runner": self.gpu_count / self.num_workers,
                "num_cpus_per_env_runner": self.cpu_count // self.num_workers,
                "num_workers": self.num_workers,
                **initial_train_params,
                "use_gpu": self.is_gpu_available
            },
            run_config=train.RunConfig(
                callbacks=[] if callbacks is None else callbacks,
                name=f"experiment_ppo_{str(job_id)}",
                stop={
                    "env_runners/episode_reward_mean": episode_reward_mean_threshold,
                    "training_iteration": max_iterations
                },
                storage_path="/app/checkpoints"
            )
        )

        # Start training/tuning.
        results = tuner.fit()
        best = results.get_best_result()

        # Export best result to dedicated directory.
        self.export_result(
            ReinforcementLearningAlgorithm.ppo,
            episode_reward_mean_threshold,
            job_id,
            best,
            max_iterations,
            environment_params
        )

        # Print gathered metrics.
        self.report_metrics(best)

        return float(best.metrics["env_runners"]["episode_reward_mean"])

    def __train_dqn(
            self,
            job_id: UUID,
            tasks: List[Task],
            concepts: List[Concept],
            environment_params: Dict[str, float],
            use_pb2: bool,
            pb2_config: Dict[str, float],
            initial_train_params: Dict[str, float],
            callbacks: Optional[List[tune.Callback]],
            num_samples: PositiveInt,
            episode_reward_mean_threshold: PositiveFloat = 95.0,
            max_iterations: PositiveInt = 100
    ) -> float:
        """
        Placeholder method for training a model using the Deep Q-Network algorithm.
        """
        print("Training DQN model...")

        pb2 = PB2(**pb2_config)

        tuner = tune.Tuner(
            "DQN",
            tune_config=tune.TuneConfig(
                metric="env_runners/episode_reward_mean",
                mode="max",
                num_samples=num_samples,
                scheduler=pb2 if use_pb2 else None,
                trial_dirname_creator=short_trial_dirname_creator
            ),
            param_space={
                "env": RecommendationEnvironment,
                "env_config": {
                    "all_concepts": concepts,
                    "all_tasks": tasks,
                    "chooseRandom": True,
                    **environment_params
                },
                "num_gpus_per_env_runner": self.gpu_count / self.num_workers,
                "num_cpus_per_env_runner": self.cpu_count // self.num_workers,
                "num_workers": self.num_workers,
                **initial_train_params,
                "use_gpu": self.is_gpu_available
            },
            run_config=train.RunConfig(
                callbacks=[] if callbacks is None else callbacks,
                name=f"experiment_dqn_{str(job_id)}",
                stop={
                    "env_runners/episode_reward_mean": episode_reward_mean_threshold,
                    "training_iteration": max_iterations
                },
                storage_path="/app/checkpoints"
            )
        )

        # Start training/tuning.
        results = tuner.fit()
        best = results.get_best_result()

        # Export best result to dedicated directory.
        self.export_result(
            ReinforcementLearningAlgorithm.dqn,
            episode_reward_mean_threshold,
            job_id,
            best,
            max_iterations,
            environment_params
        )

        # Print gathered metrics.
        self.report_metrics(best)

        return float(best.metrics["env_runners"]["episode_reward_mean"])

    @staticmethod
    def export_result(
            algorithm: ReinforcementLearningAlgorithm,
            episode_reward_mean_threshold: PositiveFloat,
            job_id: UUID,
            result: train.Result,
            max_iterations: PositiveInt,
            environment_params: Dict[str, float]
    ) -> None:
        episode_reward_mean = round(float(result.metrics["env_runners"]["episode_reward_mean"]), 2)

        directory = os.path.join("checkpoints", "best", f"{algorithm}-{episode_reward_mean}")
        os.makedirs(directory, exist_ok=True)
        Algorithm.from_checkpoint(result.checkpoint).save(directory)
        print(f"Saved algorithm to: {directory}")

        # Write metadata for better traceability.
        with open(os.path.join(directory, "meta.json"), "w") as file:
            formatted = json.dumps(
                {
                    "algorithm": algorithm,
                    "episode_reward_mean": episode_reward_mean,
                    "episode_reward_mean_threshold": episode_reward_mean_threshold,
                    "max_iterations": max_iterations,
                    "train_batch_size": int(result.metrics["config"]["train_batch_size"]),
                    "environment_params": {**environment_params},
                },
                indent=4
            )

            file.write(formatted)
            print(f"Saved metadata to: {file.name}")

        # Remove temporary checkpoint data of training.
        rmtree(os.path.join("checkpoints", f"experiment_{algorithm}_{str(job_id)}"))
        print("Removed training's checkpoint directory")

    @staticmethod
    def report_metrics(result: train.Result):
        print("Best performing trial's final reported metrics:")
        pprint.pprint({k: v for k, v in result.metrics["env_runners"].items() if k in [
            "episode_reward_mean",
            "episode_reward_max",
            "episode_reward_min",
            "episode_len_mean"
        ]})

    def run_benchmark(
            self,
            tasks: List[Task],
            concepts: List[Concept],
            episodes: PositiveInt,
            format_as: str
    ) -> Union[Dict[str, Dict[str, float]], bytes]:
        keys = ["rcr", "rpdr", "rkdr", "rpdi", "rkdi", "rb", "rd", "rs", "rn", "total"]
        rewards = {candidate: {key: [] for key in keys} for candidate in ["model", "baseline", "optimal", "direct"]}

        for i in range(episodes):
            print(f"Handling Benchmark Episode {i+1} from ", episodes)
            model_env = RecommendationEnvironment({
                "all_concepts": concepts,
                "all_tasks": tasks,
                "chooseRandom": True,
                **self.environment_params
            })

            baseline_env = deepcopy(model_env)
            optimal_env = deepcopy(model_env)
            direct_env = deepcopy(model_env)

            model_rewards = self.collect_episode_rewards(model_env)
            baseline_rewards = self.collect_episode_rewards(baseline_env, baseline=True)
            optimal_rewards = self.collect_episode_rewards(optimal_env, optimal=True)
            direct_rewards = self.collect_episode_rewards(direct_env, direct=True)

            for key in keys:
                rewards["model"][key].extend(model_rewards[key])
                rewards["baseline"][key].extend(baseline_rewards[key])
                rewards["optimal"][key].extend(optimal_rewards[key])
                rewards["direct"][key].extend(direct_rewards[key])

        averages = {key: {rk: np.mean(vals) for rk, vals in val.items()} for key, val in rewards.items()}

        return averages if format_as == "json" else RLHandler.plot_results(averages)

    def collect_episode_rewards(self, env: RecommendationEnvironment, baseline=False, optimal=False, direct=False):
        rewards = {"rcr": [], "rpdr": [], "rkdr": [], "rpdi": [], "rkdi": [], "rb": [], "rd": [], "rs": [], "rn": [], "total": []}
        obs, info = env.reset()

        continuous_action_space = env.use_cont_action_space

        done = False
        total_rewards = 0
        steps = 0
        while not done:
            if baseline:
                a = env.action_space.sample()
                if continuous_action_space:
                    action, _ = env.find_most_similar_task(a)
                    _, reward, done, _, info = env.step(action, True)
                else:
                    _, reward, done, _, info = env.step(a)
            elif optimal:
                action_rewards = []
                for i, task in enumerate(env.all_tasks):
                    usage_env = deepcopy(env)
                    if continuous_action_space:
                       _, reward, _, _, _ = usage_env.step(task, True)
                       action_rewards.append((task, reward))
                    else:
                        _, reward, _, _, _ = usage_env.step(i)
                        action_rewards.append((i, reward))
                action = max(action_rewards, key=lambda x: x[1])[0]
                _, reward, done, _, info = env.step(action, True)
            elif direct:
                if continuous_action_space:
                    action = self.model.compute_single_action(obs)
                    _, reward, done, _, info = env.step(action, False)
                else:
                    # Skip direct value computation for discrete action space
                    done = True
                    continue
            else:
                a = self.model.compute_single_action(obs)
                if continuous_action_space:
                    action, _ = env.find_most_similar_task(a)
                    _, reward, done, _, info = env.step(action, True)
                else:
                    _, reward, done, _, info = env.step(a, False)

            if not continuous_action_space:
                rewards["rn"].append(info["reward_new"])
            rewards["rcr"].append(info["concept_relevance_reward"])
            rewards["rpdr"].append(info["pd_relevance_reward"])
            rewards["rkdr"].append(info["kd_relevance_reward"])
            rewards["rpdi"].append(info["pd_improvement_reward"])
            rewards["rkdi"].append(info["kd_improvement_reward"])
            rewards["rb"].append(info["bonus_reward"])
            rewards["rd"].append(info["reward_difficulty"])
            rewards["rs"].append(info["reward_stress"])

            #total_rewards += reward
            rewards['total'].append(reward)
            steps += 1

        #rewards['total'].append(total_rewards / steps)
        return rewards

    @staticmethod
    def plot_results(averages: Dict[str, Dict[str, float]]) -> bytes:
        continuous_action_space = averages["direct"]["total"] != []

        if continuous_action_space:
            labels = ["RCR", "RPDR", "RKDR", "RPDI", "RKDI", "RB", "RD", "RS"]
        else:
            labels = ["RCR", "RPDR", "RKDR", "RPDI", "RKDI", "RB", "RD", "RS", "RN"]
        model_values = [averages["model"][k.lower()] for k in labels]
        direct_values = [averages["direct"][k.lower()] for k in labels]
        baseline_values = [averages["baseline"][k.lower()] for k in labels]
        optimal_values = [averages["optimal"][k.lower()] for k in labels]

        fig, axs = plt.subplots(1, 2, figsize=(12, 6))
        bar_width = 0.2

        if continuous_action_space:
            r1 = np.arange(len(labels))
            r2 = [x + bar_width for x in r1]
            r3 = [x + bar_width for x in r2]
            r4 = [x + bar_width for x in r3]

            # Performance comparison per label.
            axs[0].bar(r1, model_values, color="blue", width=bar_width, edgecolor="grey", label="Loaded Model")
            axs[0].bar(r2, direct_values, color='green', width=bar_width, edgecolor="grey", label="Abstract Task")
            axs[0].bar(r3, baseline_values, color="orange", width=bar_width, edgecolor="grey", label="Baseline")
            axs[0].bar(r4, optimal_values, color="tomato", width=bar_width, edgecolor="grey", label="Greedy")
            axs[0].set_xlabel("Reward Types", fontweight="bold")
            axs[0].set_xticks([r + bar_width for r in range(len(labels))])
            axs[0].set_xticklabels(labels)
            axs[0].legend()

            # Total performance comparison.
            model_avg = averages["model"]["total"]
            direct_avg = averages["direct"]["total"]
            baseline_avg = averages["baseline"]["total"]
            optimal_avg = averages["optimal"]["total"]

            labels_right = ["Loaded Model", "Abstract Task", "Baseline", "Greedy"]
            axs[1].bar(labels_right[0], model_avg, color="blue")
            axs[1].bar(labels_right[1], direct_avg, color="green")
            axs[1].bar(labels_right[2], baseline_avg, color="orange")
            axs[1].bar(labels_right[3], optimal_avg, color="tomato")
        else:
            r1 = np.arange(len(labels))
            r2 = [x + bar_width for x in r1]
            r3 = [x + bar_width for x in r2]

            # Performance comparison per label.
            axs[0].bar(r1, model_values, color="blue", width=bar_width, edgecolor="grey", label="Loaded Model")
            axs[0].bar(r2, baseline_values, color="orange", width=bar_width, edgecolor="grey", label="Baseline")
            axs[0].bar(r3, optimal_values, color="tomato", width=bar_width, edgecolor="grey", label="Greedy")
            axs[0].set_xlabel("Reward Types", fontweight="bold")
            axs[0].set_xticks([r + bar_width for r in range(len(labels))])
            axs[0].set_xticklabels(labels)
            axs[0].legend()

            # Total performance comparison.
            model_avg = averages["model"]["total"]
            baseline_avg = averages["baseline"]["total"]
            optimal_avg = averages["optimal"]["total"]

            labels_right = ["Loaded Model", "Baseline", "Greedy"]
            axs[1].bar(labels_right[0], model_avg, color="blue")
            axs[1].bar(labels_right[1], baseline_avg, color="orange")
            axs[1].bar(labels_right[2], optimal_avg, color="tomato")

        axs[1].set_ylabel("Average Total Reward")
        axs[1].set_xlabel("Algorithms", fontweight="bold")
        axs[1].legend()

        plt.tight_layout()

        with io.BytesIO() as buffer:
            plt.savefig(buffer, format="png")
            buffer.seek(0)
            image = buffer.getvalue()

        plt.close()

        return image

    def load_model_from_checkpoint(self, path_to_checkpoint) -> None:
        self.model = Algorithm.from_checkpoint(path_to_checkpoint)
        self.loaded_model_path = path_to_checkpoint

    def unload_model(self) -> None:
        self.model = None
        self.loaded_model_path = None

    def recommend_task(self, env) -> (Task, float, Task):
        obs, info = env.reset()
        action = self.model.compute_single_action(obs)
        if env.use_cont_action_space:
            task, sim = env.find_most_similar_task(action)
            ideal_task = env.create_abstract_task_from_action(action)
        else:
            task = env.all_tasks[action]
            sim = None
            ideal_task = None
        return task, sim, ideal_task

###############################################################################
# EXAMPLE USAGE
###############################################################################
# rl_handler = RLHandler()
# db_handler = DBHandler(host="localhost", database="admin", user="admin", password="admin")
# db_handler.connect()

# tasks = db_handler.fetch_tasks()
# concepts = db_handler.fetch_all_concepts()

# rl_handler.load_model_from_checkpoint("../checkpoints/best/ppo-26.70")
# rl_handler.run_benchmark(tasks, concepts, 10)

# rl_handler.train_ppo("test", tasks, concepts)
# rl_handler.train_dqn("test", tasks, concepts)

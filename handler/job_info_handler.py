from datetime import datetime
from typing import Dict
from uuid import UUID

from pydantic import PositiveInt, PositiveFloat

from classes import JobStatus, ReinforcementLearningAlgorithm


class JobInfoHandler:
    """
    Provides utility methods for handling job information.
    """

    def __init__(self):
        self.__jobs: Dict[str, Dict[str, object]] = {}

    def add(
            self,
            job_id: UUID,
            algorithm: ReinforcementLearningAlgorithm,
            episode_reward_mean_threshold: PositiveFloat,
            max_iterations: PositiveInt
    ) -> None:
        self.__jobs[str(job_id)] = {
            "algorithm": algorithm,
            "created_at": datetime.now().strftime("%Y-%m-%dT%H:%M:%S%z"),
            "episode_reward_mean_threshold": episode_reward_mean_threshold,
            "max_iterations": max_iterations,
            "status": JobStatus.pending
        }

    def get(self, job_id: UUID) -> Dict[str, object]:
        return self.__jobs.get(str(job_id), {"error": "Invalid job id entered"})

    def finalize_error(self, job_id: UUID, error: str, report_iteration: bool = False) -> None:
        if self.__jobs[str(job_id)]["status"] not in [JobStatus.pending, JobStatus.running]:
            return

        if not report_iteration and self.__jobs[str(job_id)]["status"] == JobStatus.running:
            self.__jobs[str(job_id)].pop("iteration")

        self.__jobs[str(job_id)].update({
            "error": error,
            "status": JobStatus.failed,
            "updated_at": datetime.now().strftime("%Y-%m-%dT%H:%M:%S%z")
        })

    def finalize_success(self, job_id: UUID, episode_reward_mean: PositiveFloat) -> None:
        if self.__jobs[str(job_id)]["status"] != JobStatus.running:
            return

        self.__jobs[str(job_id)].pop("iteration")

        self.__jobs[str(job_id)].update({
            "episode_reward_mean": round(episode_reward_mean, 2),
            "status": JobStatus.completed,
            "updated_at": datetime.now().strftime("%Y-%m-%dT%H:%M:%S%z")
        })

import hashlib
from typing import Optional

from influxdb_client import InfluxDBClient
from influxdb_client.client.exceptions import InfluxDBError


class InfluxDBHandler:
    """
    Handles InfluxDB operations.

    Attributes:
        url (str): The URL of the InfluxDB server.
        token (str): The authentication token.
        org (str): The organization to use.
        bucket (str): The bucket to query.
        client (InfluxDBClient): The InfluxDB client instance.
    """

    def __init__(self, url: str, token: str, org: str, bucket: str):
        """
        Initializes a new instance of the InfluxDBHandler class.

        Parameters:
            url (str): The URL of the InfluxDB server.
            token (str): The authentication token.
            org (str): The organization in InfluxDB.
            bucket (str): The bucket to operate on.
        """
        self.url = url
        self.token = token
        self.org = org
        self.bucket = bucket
        self.client: InfluxDBClient = None

    def connect(self):
        """
        Establishes a connection to the InfluxDB server.
        """
        self.client = InfluxDBClient(url=self.url, token=self.token, org=self.org)
        print("InfluxDB connection established")

    def close(self):
        """
        Closes the InfluxDB connection.
        """
        if self.client:
            self.client.close()
            print("InfluxDB connection closed")

    def fetch_level_by_email(self, email: str) -> Optional[float]:
        """
        Hashes the provided email using SHA-384, queries the specified bucket for
        the measurement (named as the hash) and averages the 'level' field over the
        last 30 seconds.

        Parameters:
            email (str): The email address to process.

        Returns:
            float: The averaged 'level' value over the last 30 seconds.
                   Returns None if no data is found.
        """
        if self.client is None:
            print("No InfluxDB connection. Call connect() first.")
            return None

        hashed_email = hashlib.sha384(email.encode("utf-8")).hexdigest()
        flux_query = f'''
        from(bucket: "{self.bucket}")
          |> range(start: -1m)
          |> filter(fn: (r) => r._measurement == "{hashed_email}")
          |> filter(fn: (r) => r["_field"] == "level")
          |> mean()
        '''

        query_api = self.client.query_api()
        try:
            result = query_api.query(flux_query, org=self.org)
            for table in result:
                for record in table.records:
                    value = record.get_value()
                    if isinstance(value, (int, float)):
                        return float(value)
            return None
        except InfluxDBError as e:
            print(f"Failed to fetch level data: {e}")
            return None


###############################################################################
# EXAMPLE USAGE
###############################################################################
#if __name__ == '__main__':
#    influx_handler = InfluxDBHandler(
#        url="http://localhost:8086",
#        token="your-token-here",
#        org="your-org-here",
#        bucket="your-bucket-here"
#    )
#    influx_handler.connect()
#
#    email = "user@example.com"
#    level = influx_handler.fetch_level_by_email(email)
#    print(f"Level for {email}: {level}")
#
#    influx_handler.close()
